# ProvablyFair

Provably Fair .Net Generator

# Usage
```
var serverKey = ProvablyFairGenerator.GetServerKey();

ProvablyFairGenerator.Result result = ProvablyFairGenerator.RollDice(serverKey, "userKey");
```

# Donation
Support from anyone is much appreciated!  
Bitcoin: `1DX5dqq5N8mFqPRrLeANhqDs5ShwP15xTv`