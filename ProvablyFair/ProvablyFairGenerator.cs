﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ProvablyFair
{
    public static class ProvablyFairGenerator
    {
        private static long nonce;
        private static object lockObj = new object();

        public static byte[] GetServerKey()
        {
            var _serverKey = new byte[128];
            nonce = 0;
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(_serverKey);
            }
            using (var sha = SHA256.Create())
            {
                return sha.ComputeHash(_serverKey);
            }
        }

        public static Result RollDice(byte[] serverKey, string userKey)
        {
            if (nonce == long.MaxValue)
            {
                throw new InvalidOperationException("Ran out of Nonce values, you must start a new session.");
            }

            using (var hmac = new HMACSHA256(serverKey))
            {
                float? roll = null;
                string message = null;
                while (roll == null)
                {
                    lock (lockObj)
                    {
                        message = userKey + "-" + nonce;
                        nonce++;
                    }

                    var data = Encoding.UTF8.GetBytes(message);
                    var hash = hmac.ComputeHash(data);
                    roll = GetNumberFromByteArray(hash);
                }
                return new Result(message, roll.Value);
            }
        }

        private static float? GetNumberFromByteArray(byte[] hash)
        {
            var hashString = string.Join("", hash.Select(x => x.ToString("X2")));
            const int chars = 5;
            for (int i = 0; i <= hashString.Length - chars; i += chars)
            {
                var substring = hashString.Substring(i, chars);
                var number = int.Parse(substring, System.Globalization.NumberStyles.HexNumber);
                if (number > 999999)
                    continue;
                return (number % 10000) / 100.0f;
            }
            return null;
        }

        public class Result
        {
            public Result(string hmacMessage, float roll)
            {
                HmacMessage = hmacMessage;
                Roll = roll;
            }

            public string HmacMessage { get; }
            public float Roll { get; }
        }
    }
}